package io.nunomota.adobe.handler;

import io.nunomota.adobe.handler.auxiliary.ClientConnection;
import io.nunomota.adobe.handler.auxiliary.History;
import io.nunomota.adobe.handler.auxiliary.Request;
import io.nunomota.adobe.handler.auxiliary.RequestStatus;
import io.nunomota.adobe.handler.factory.ResponseFactory;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This class handles HTTP/1.1 client requests'
 * execution. Although easily extensible, it
 * returns a default HTML page as a functional
 * example.
 *
 * Created by nunomota on 7/3/2018.
 */
public class Worker implements Runnable {

    private final static Logger LOGGER = Logger.getLogger(Worker.class);

    private final Request clientRequest;

    Worker(Request clientRequest) {
        this.clientRequest = clientRequest;
    }

    /**
     * This method takes care of the class' main
     * logic. It attempts to handle a persistent
     * {@link io.nunomota.adobe.handler.auxiliary.ClientConnection}.
     */
    public void run() {
        LOGGER.debug("Executing worker thread");
        try {
            ClientConnection clientConnection = new ClientConnection(this.clientRequest);
            LOGGER.debug(String.format("%sPersistent connection detected", (clientConnection.isPersistent)?"":"Non-"));
            do {
                String request = clientConnection.read();
                LOGGER.debug(String.format("Received request: %s", request));
                String response = ResponseFactory.FromRequest(request);
                LOGGER.debug(String.format("Sending response: %s", response));
                clientConnection.write(response);
            } while (clientConnection.isPersistent);
            clientConnection.close();
        } catch (IOException e) {
            LOGGER.error("[IOException] Could not handle client's request");
            PrintWriter printWriter = new PrintWriter(new StringWriter());
            e.printStackTrace(printWriter);
            LOGGER.error(printWriter.toString());
        }
        LOGGER.debug("Finishing worker thread execution");
        this.clientRequest.updateStatus(RequestStatus.SUCCEEDED);
        History.SINGLETON.update(this.clientRequest);
    }
}
