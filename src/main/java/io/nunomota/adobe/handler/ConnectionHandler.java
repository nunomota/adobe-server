package io.nunomota.adobe.handler;

import io.nunomota.adobe.handler.auxiliary.History;
import io.nunomota.adobe.handler.auxiliary.Request;
import org.apache.log4j.Logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * This class handles incoming client connections
 * in a multi-threaded fashion. It assigns every
 * new request to the next available thread in
 * an initialized thread-pool. By being ran in its
 * own thread it can be halted - at any point -
 * through interrupt signals (allowing it to properly
 * free resources before exiting).
 *
 * Created by nunomota on 7/3/2018.
 */
public class ConnectionHandler extends Thread {

    private static final int DEFAULT_PORT = 9000;
    private static final int DEFAULT_TIMEOUT = 5000;
    private static final Logger LOGGER = Logger.getLogger(ConnectionHandler.class);

    private Boolean isRunning;
    private ServerSocket serverSocket;
    private ExecutorService threadPool;
    private ArrayList<Future> scheduledTasks;

    /**
     * Alternative constructor with default port
     * value assignment ({@link io.nunomota.adobe.handler.ConnectionHandler#DEFAULT_PORT}).
     */
    public ConnectionHandler() {
        this(ConnectionHandler.DEFAULT_PORT);
    }

    /**
     * Class' constructor. Allocates necessary resources and
     * handles initialization failure.
     *
     * @param port local port to listen on for incoming requests
     */
    public ConnectionHandler(int port) {
        LOGGER.debug(String.format("Initializing connection io.nunomota.adobe.handler on port '%s'", port));
        this.isRunning = false;
        this.threadPool = Executors.newFixedThreadPool(15);
        this.scheduledTasks = new ArrayList<Future>();
        try {
            this.serverSocket = new ServerSocket(port);
            this.serverSocket.setSoTimeout(ConnectionHandler.DEFAULT_TIMEOUT);
        } catch (IOException e) {
            LOGGER.error("[IOException] Could not initialize server socket");
            PrintWriter printWriter = new PrintWriter(new StringWriter());
            e.printStackTrace(printWriter);
            LOGGER.error(printWriter.toString());
        }
    }

    /**
     * This method handles the class' main logic and
     * delegates clients' request execution to
     * asynchronous {@link io.nunomota.adobe.handler.Worker}. By checking
     * for interrupt signals at a {@link io.nunomota.adobe.handler.ConnectionHandler#DEFAULT_TIMEOUT}
     * millisecond interval it allows for proper interrupt
     * signal handling.
     */
    public void run() {
        this.isRunning = true;
        while(this.isRunning) {
            Request clientRequest = null;
            try {
                LOGGER.debug("Waiting for client connection");
                Socket clientSocket = this.serverSocket.accept();
                clientRequest = new Request(clientSocket);
            } catch (SocketTimeoutException e) {
                /* Differentiate timeouts from acceptance failure.
                 * We do not want to handle timeouts since we are
                 * enforcing them to check for this thread's
                 * interruption.
                 */
            } catch (IOException e) {
                LOGGER.debug("[IOException] Could not accept request");
                PrintWriter printWriter = new PrintWriter(new StringWriter());
                e.printStackTrace(printWriter);
                LOGGER.error(printWriter.toString());
            }

            if (Thread.interrupted()) {
                LOGGER.debug("Interrupt signal detected");
                this.isRunning = false;
                cleanupTasks();
            } else {
                if (clientRequest == null) continue;
                LOGGER.debug("Adding newly received request to pool");
                History.SINGLETON.add(clientRequest);
                Future newTask = this.threadPool.submit(new Worker(clientRequest));
                this.scheduledTasks.add(newTask);
            }
        }
    }

    /**
     * This method handles resource de-allocation
     * and handles {@link io.nunomota.adobe.handler.Worker} instances
     * to shutdown.
     */
    public void close() {
        LOGGER.debug("Closing connection io.nunomota.adobe.handler");
        cleanupTasks();
        this.threadPool.shutdown();
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            LOGGER.debug("[IOException] Could not close server socket");
            PrintWriter printWriter = new PrintWriter(new StringWriter());
            e.printStackTrace(printWriter);
            LOGGER.error(printWriter.toString());
        }
    }

    /**
     * This method is used to force {@link io.nunomota.adobe.handler.Worker}
     * running instances' shutdown.
     */
    private void cleanupTasks() {
        LOGGER.debug(String.format("Canceling %d scheduled tasks", this.scheduledTasks.size()));
        for (Future scheduledTask : this.scheduledTasks) {
            scheduledTask.cancel(true);
        }
        this.scheduledTasks.clear();
    }
}
