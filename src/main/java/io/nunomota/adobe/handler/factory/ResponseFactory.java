package io.nunomota.adobe.handler.factory;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Scanner;

/**
 * This class is responsible for generating HTML
 * responses based on an HTTP GET request. Since
 * the generated return values will be HTML pages
 * fetched at the requested location, this class
 * is also protected against relative path navigation
 * attacks (preventing a user from exploring files
 * outside of the designated root directory for
 * HTML webpages).
 *
 * Created by nunomota on 7/4/2018.
 */
public class ResponseFactory {

    private static final Logger LOGGER = Logger.getLogger(ResponseFactory.class);
    private static final HtmlCompressor HTML_COMPRESSOR = new HtmlCompressor();
    private static final String GET_REQUEST_REGEX = "GET [/[A-Za-z0-9_]*]+(.html|.png)? HTTP/1.1";
    private static final String ROOT_DIRECTORY_HTML = "pages";

    private static final String DEFAULT_RESPONSE_SUCCESS =
            "HTTP/1.1 200 OK\r\n" +
            "Content-Length: %d\r\n" +
            "Content-Type: text/html\r\n\r\n%s";
    private static final String DEFAULT_RESPONSE_FAVICON =
            "HTTP/1.1 200 OK\r\n" +
            "Content-Length: %d\r\n" +
            "Content-Type: image/png\r\n\r\n%s";
    private static final String DEFAULT_RESPONSE_MALFORMED = "HTTP/1.1 400 Bad Request";
    private static final String DEFAULT_RESPONSE_NOT_FOUND = "HTTP/1.1 404 Not Found";
    private static final String DEFAULT_RESPONSE_ERROR = "HTTP/1.1 500 Internal io.nunomota.adobe.Server Error";

    private static final String DEFAULT_RESPONSE_CONTENT = "%s\r\n";

    private ResponseFactory() {}

    /**
     * This method generates a valid HTTP/1.1 response
     * for a given HTTP/1.1 GET request. It handles both
     * valid and invalid requests (and handles certain
     * security features, like relative path navigation).
     *
     * @param request HTTP/1.1 string-formatted GET request
     * @return string containing the appropriate HTTP/1.1 response
     */
    public static String FromRequest(String request) {
        LOGGER.debug(String.format("Parsing request '%s'", request));
        if (!request.matches(ResponseFactory.GET_REQUEST_REGEX)) {
            LOGGER.debug(String.format("Request '%s' does not match regex", request));
            return ResponseFactory.DEFAULT_RESPONSE_MALFORMED;
        }

        String targetFile = request.split("\\s+")[1];
        try {
            LOGGER.debug(String.format("Retrieving page '%s'", targetFile));
            if (targetFile.equals("/favicon.png")) {
                byte[] faviconContent = ResponseFactory.FetchFavicon(targetFile);
                String responseContent = String.format(ResponseFactory.DEFAULT_RESPONSE_CONTENT, faviconContent.toString());
                return String.format(ResponseFactory.DEFAULT_RESPONSE_FAVICON, responseContent.getBytes("UTF-8").length-1, responseContent);
            } else {
                String targetFileContent = HTML_COMPRESSOR.compress(ResponseFactory.FetchFile(targetFile));
                String responseContent = String.format(ResponseFactory.DEFAULT_RESPONSE_CONTENT, targetFileContent);
                return String.format(ResponseFactory.DEFAULT_RESPONSE_SUCCESS, responseContent.getBytes("UTF-8").length-1, responseContent);
            }
        } catch (NullPointerException|FileNotFoundException e) {
            LOGGER.debug(String.format("Could not find page '%s'", targetFile));
            return ResponseFactory.DEFAULT_RESPONSE_NOT_FOUND;
        } catch (UnsupportedEncodingException e) {
            LOGGER.debug("Unsupported encoding detected");
            return ResponseFactory.DEFAULT_RESPONSE_ERROR;
        }
    }

    /**
     * This method retrieves the favicon for an HTML page as a
     * byte array.
     *
     * @param filePath path to favicon
     * @return byte array with favicon's file binary data
     * @throws FileNotFoundException thrown when specified favicon file does not exist
     */
    private static byte[] FetchFavicon(String filePath) throws FileNotFoundException {
        String fullPath = String.format("%s%s", ROOT_DIRECTORY_HTML, filePath);
        ClassLoader classLoader = ResponseFactory.class.getClassLoader();
        try {
            return IOUtils.toByteArray(classLoader.getResourceAsStream(fullPath));
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }

    /**
     * This method retrieves an HTML page as a simple string.
     *
     * @param filePath path to target HTML page
     * @return string-formatted HTML code
     * @throws NullPointerException thrown when HTML page cannot be read
     * @throws FileNotFoundException thrown when HTML page does not exist
     */
    private static String FetchFile(String filePath) throws NullPointerException, FileNotFoundException {
        String fullPath = String.format("%s%s", ROOT_DIRECTORY_HTML, filePath);
        String fixedPath = ResponseFactory.FixFilePath(fullPath);
        ClassLoader classLoader = ResponseFactory.class.getClassLoader();
        File file = new File(classLoader.getResource(fixedPath).getFile());
        Scanner scanner = new Scanner(file);
        scanner.useDelimiter("\\Z");
        return scanner.next();
    }

    /**
     * This method takes care of fixing certain requests'
     * formatting.
     *
     * @param filePath path to be analysed
     * @return new path with valid formatting
     */
    private static String FixFilePath(String filePath) {
        if (filePath.endsWith("/")) return String.format("%s%s", filePath, "index.html");
        if (!filePath.endsWith(".html")) return String.format("%s.html", filePath);
        return filePath;
    }
}
