package io.nunomota.adobe.handler.auxiliary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import org.apache.log4j.Logger;

/**
 * This class is used as an intermediary for easy
 * read/write operations on a persistent socket
 * connection.
 *
 * Created by nunomota on 7/4/2018.
 */
public class ClientConnection {

    private final static Logger LOGGER = Logger.getLogger(ClientConnection.class);

    public final Boolean isPersistent;
    private final Request clientRequest;

    public ClientConnection(Request clientRequest) throws IOException {
        this.clientRequest = clientRequest;
        this.isPersistent = clientRequest.getClientSocket().getKeepAlive();
    }

    /**
     * This method takes care of reading a socket's
     * input buffer (reading a client-side request).
     *
     * @return string-formatted client request
     * @throws IOException thrown when input cannot be read from socket
     */
    public String read() throws IOException {
        LOGGER.debug("Reading request from socket");
        InputStreamReader inStream = new InputStreamReader(this.clientRequest.getClientSocket().getInputStream());
        BufferedReader inBuffer = new BufferedReader(inStream);
        return inBuffer.readLine();
    }

    /**
     * This method takes care of writing to a socket's
     * output buffer (sending a server-side response).
     *
     * @param message string-formatted server-side response
     */
    public void write(String message) throws IOException {
        LOGGER.debug("Writing response to socket");
        PrintWriter outBuffer = new PrintWriter(this.clientRequest.getClientSocket().getOutputStream(), true);
        outBuffer.write(message);
        outBuffer.flush();
    }

    /**
     * This method handles I/O buffer and socket closing.
     *
     * @throws IOException thrown when resources cannot be freed
     */
    public void close() throws IOException {
        LOGGER.debug("Closing client connection");
        this.clientRequest.getClientSocket().close();
    }
}
