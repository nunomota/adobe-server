package io.nunomota.adobe.handler.auxiliary;

import org.apache.log4j.Logger;
import java.util.ArrayList;

/**
 * This class is meant to be a stateful information container.
 * It can be updated with {@link io.nunomota.adobe.handler.auxiliary.Request}
 * for client-side request historical monitoring.
 *
 * Created by nunomota on 7/3/2018.
 */
public class History {

    private static final Logger LOGGER = Logger.getLogger(History.class);

    public static final History SINGLETON = new History();

    private int requestsReceived;
    private int requestsFulfilled;
    private int requestsFailed;
    private ArrayList<Request> activeRequests;

    private History() {
        this.requestsReceived = 0;
        this.requestsFulfilled = 0;
        this.requestsFailed = 0;
        this.activeRequests = new ArrayList<Request>();
    }

    /**
     * This method adds a new {@link io.nunomota.adobe.handler.auxiliary.Request}
     * to a list of currently active requests.
     *
     * @param newRequest new {@link io.nunomota.adobe.handler.auxiliary.Request} to add to currently active requests
     */
    public void add(Request newRequest) {
        LOGGER.debug(String.format("Adding request '%s' to history", newRequest.toString()));
        this.activeRequests.add(newRequest);
        this.requestsReceived += 1;
    }

    /**
     * This method updates historical data according
     * a given {@link io.nunomota.adobe.handler.auxiliary.Request}'s
     * new {@link io.nunomota.adobe.handler.auxiliary.RequestStatus}.
     *
     * @param targetRequest {@link io.nunomota.adobe.handler.auxiliary.Request} to check for changes
     */
    public void update(Request targetRequest) {
        LOGGER.debug(String.format("Updating request '%s'", targetRequest.toString()));
        if (activeRequests.contains(targetRequest)) {
            RequestStatus newStatus = targetRequest.getStatus();
            LOGGER.debug(String.format("New status '%s' for request '%s'", newStatus.name(), targetRequest.toString()));
            switch (newStatus) {
                case SUCCEEDED:
                    this.requestsFulfilled += 1;
                    this.activeRequests.remove(targetRequest);
                    break;
                case FAILED:
                    this.requestsFailed += 1;
                    this.activeRequests.remove(targetRequest);
                    break;
                case RECEIVED:
                    LOGGER.debug(String.format("Calling update on non-finished request '%s'", targetRequest.toString()));
                    break;
                default:
                    LOGGER.debug(String.format("Calling update on weirdly formatted request '%s'", targetRequest.toString()));
                    break;
            }
        } else {
            LOGGER.debug(String.format("Calling update on non-existing request '%s'", targetRequest.toString()));
        }
    }

    public String toString() {
        return String.format(
                "[Received: %d | Succeeded: %d | Failed: %d | Ongoing: %d]",
                this.requestsReceived,
                this.requestsFulfilled,
                this.requestsFailed,
                this.activeRequests.size()
        );
    }
}
