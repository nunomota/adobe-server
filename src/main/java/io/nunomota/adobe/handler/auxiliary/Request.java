package io.nunomota.adobe.handler.auxiliary;

import java.net.Socket;
import org.apache.log4j.Logger;

/**
 * This class is meant to be a stateful information container
 * for received client-side requests.
 *
 * Created by nunomota on 7/3/2018.
 */
public class Request {

    private static final Logger LOGGER = Logger.getLogger(Request.class);

    private final Socket clientRequest;
    private RequestStatus status;

    public Request(Socket clientRequest) {
        this.clientRequest = clientRequest;
        this.status = RequestStatus.RECEIVED;
    }

    /**
     * This takes care of updating an instance's
     * {@link io.nunomota.adobe.handler.auxiliary.RequestStatus}.
     *
     * @param newStatus new {@link io.nunomota.adobe.handler.auxiliary.RequestStatus}
     */
    public void updateStatus(RequestStatus newStatus) {
        LOGGER.debug(String.format("Changing request status (%s > %s)", this.status, newStatus));
        this.status = newStatus;
    }

    /**
     * Package-private getter method.
     *
     * @return current {@link io.nunomota.adobe.handler.auxiliary.RequestStatus}
     */
    RequestStatus getStatus() {
        return this.status;
    }

    /**
     * Package-private getter method.
     *
     * @return client's connection socket
     */
    Socket getClientSocket() {
        return this.clientRequest;
    }
}
