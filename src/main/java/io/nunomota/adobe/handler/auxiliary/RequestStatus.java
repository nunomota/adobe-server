package io.nunomota.adobe.handler.auxiliary;

/**
 * This enum represents all possible states
 * for a {@link io.nunomota.adobe.handler.auxiliary.Request} instance.
 *
 * Created by nunomota on 7/3/2018.
 */
public enum RequestStatus {
    RECEIVED,
    FAILED,
    SUCCEEDED;
}
