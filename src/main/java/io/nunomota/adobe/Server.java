package io.nunomota.adobe;

import io.nunomota.adobe.controller.LogicController;
import io.nunomota.adobe.controller.LogicControllerOptions;
import io.nunomota.adobe.handler.ConnectionHandler;
import io.nunomota.adobe.handler.auxiliary.History;
import org.apache.log4j.Logger;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This class controls the server's
 * main execution loop.
 *
 * Created by nunomota on 7/3/2018.
 */
public class Server {

    private final static Logger LOGGER = Logger.getLogger(Server.class);

    public static void main(String[] args) {
        LogicController logicController = new LogicController();
        ConnectionHandler connectionHandler = null;

        Boolean isRunning = true;
        while(isRunning) {
            LogicControllerOptions selectedOption = logicController.run();
            LOGGER.debug(String.format("Executing option '%s'", selectedOption.label_long));
            switch (selectedOption) {
                case RUN:
                    if (connectionHandler == null) {
                        connectionHandler = new ConnectionHandler();
                        connectionHandler.start();
                    }
                    break;
                case STOP:
                    if (connectionHandler != null) {
                        HaltServer(connectionHandler);
                        connectionHandler = null;
                    }
                    break;
                case HIST:
                    System.out.println(History.SINGLETON.toString());
                    break;
                case EXIT:
                    if (connectionHandler != null) {
                        HaltServer(connectionHandler);
                        connectionHandler = null;
                    }
                    isRunning = false;
                    break;
                default:
                    LOGGER.debug("Unknown option received");
            }
        }
        LOGGER.debug("Successfully exited application");
    }

    /**
     * This method handles server halting and resource
     * freeing. It sends an interrupt signal to the
     * multi-threaded connection io.nunomota.adobe.handler ({@link io.nunomota.adobe.handler.ConnectionHandler})
     * and waits for it to terminate non-forcefully.
     *
     * @param connectionHandler (non-null) {@link io.nunomota.adobe.handler.ConnectionHandler} instance to interrupt
     */
    private static void HaltServer(ConnectionHandler connectionHandler) {
        LOGGER.debug("Halting server");
        if (connectionHandler == null) return;
        LOGGER.debug("Sending interrupt signal to connection io.nunomota.adobe.handler");
        connectionHandler.interrupt();
        try {
            LOGGER.debug("Waiting for connection io.nunomota.adobe.handler to exit");
            connectionHandler.join();
        } catch (InterruptedException e) {
            LOGGER.debug("[InterruptedException] Could not join thread");
            PrintWriter printWriter = new PrintWriter(new StringWriter());
            e.printStackTrace(printWriter);
            LOGGER.error(printWriter.toString());
        }
        connectionHandler.close();
    }
}
