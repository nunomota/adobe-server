package io.nunomota.adobe.controller;

/**
 * This enum represents all possible options
 * to be presented by a {@link io.nunomota.adobe.controller.LogicController}
 * instance.
 *
 * Created by nunomota on 7/3/2018.
 */
public enum LogicControllerOptions {

    RUN(0, "Run", "R", "Starts server execution"),
    STOP(1, "Stop", "S", "Halts io.nunomota.adobe.Server execution"),
    HIST(2, "Hist", "H", "Displays request history summary"),
    EXIT(3, "Exit", "E", "Exit application");

    public final int code;
    public final String label_long;
    public final String label_short;
    public final String description;

    LogicControllerOptions(int code, String label_long, String label_short, String description) {
        this.code = code;
        this.label_long = label_long;
        this.label_short = label_short;
        this.description = description;
    }

    /**
     * This method converts string-encoded {@link io.nunomota.adobe.controller.LogicControllerOptions}
     * to their enum values' counterpart.
     *
     * @param label_short string corresponding to a {@link io.nunomota.adobe.controller.LogicControllerOptions#label_short}
     * @return {@link io.nunomota.adobe.controller.LogicControllerOptions} enum value
     */
    public static LogicControllerOptions FromShortLabel(String label_short) {
        for (LogicControllerOptions option : LogicControllerOptions.values()) {
            if (option.label_short.equals(label_short)) {
                return option;
            }
        }
        return null;
    }
}
