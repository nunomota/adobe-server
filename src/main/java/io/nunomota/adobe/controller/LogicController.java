package io.nunomota.adobe.controller;

import java.util.Scanner;
import org.apache.log4j.Logger;

/**
 * This class handles user-interaction and interface
 * through a menu. Any knowledge of a valid,
 * user-required operation is then back-propagated to
 * the main server.
 *
 * Created by nunomota on 7/3/2018.
 */
public class LogicController {

    private static final Logger LOGGER = Logger.getLogger(LogicController.class);

    /**
     * This method handles the class' main logic.
     * It presents the user with a menu, waits
     * for new input and validates it before
     * informing the main server instance.
     *
     * @return (non-null) {@link io.nunomota.adobe.controller.LogicControllerOptions} specified by the user
     */
    public LogicControllerOptions run() {
        String header = getMenuHeader();
        String entries = getMenuEntries();

        Scanner in = new Scanner(System.in);
        String warning = null;
        while (true) {
            System.out.println(header);
            System.out.println(entries);
            if (warning != null) {
                System.out.println(warning);
            }
            LOGGER.debug("Waiting for user input");
            System.out.print(">> Input: ");
            String input = in.nextLine();

            if (input.matches("[A-Za-z]{1}")) {
                LogicControllerOptions selectedOption = LogicControllerOptions.FromShortLabel(input);
                if (selectedOption != null) {
                    LOGGER.debug(String.format("Identified user-selected option '%s'", selectedOption.label_short));
                    return selectedOption;
                }
                LOGGER.debug(String.format("Invalid user-selected option '%s'", input));
            }
            warning = "[HINT] Use initial upper-case letters";
        }
    }

    /**
     * This method creates an ASCII-art based
     * menu header.
     *
     * @return string-formatted menu header
     */
    private String getMenuHeader() {
        return  "           ________\n" +
                "==========|__MENU__|==========";
    }

    /**
     * This method creates ASCII-art based menu
     * entries according to available {@link io.nunomota.adobe.controller.LogicControllerOptions}.
     *
     * @return string-formatted menu entries
     */
    private String getMenuEntries() {
        StringBuilder menuEntries = new StringBuilder();
        for (LogicControllerOptions option : LogicControllerOptions.values()) {
            String entryLine = String.format(
                    ">> (%s)%-8s %-80s\n",
                    option.label_short,
                    option.label_long.substring(1),
                    option.description
            );
            menuEntries.append(entryLine);
        }
        menuEntries.append("==============================\n");
        return menuEntries.toString();
    }
}
