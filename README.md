# Web Tech Test

A simple multi-threaded, thread-pooled HTTP/1.1 server. This approach handles *keep-alive* header parameters if present, otherwise fulfilling requests by returning the requested resources through valid HTTP/1.1 responses. This server is expecting HTTP/1.1 GET requests matching the regular expression `GET [/[A-Za-z0-9_]*]+(.html|.png)? HTTP/1.1`. Simply put, it:

* Prevents *NULL* resource requests (`[/[A-Za-z0-9_]*]+` forces a `/` to at least appear once)
* Prevents relative directory navigation attacks (whatever directory the server uses as **root** to search for HTML pages, the client should never have access to any directory outside of that, hence `[A-Za-z0-9_]` doesn't allow for things like `GET /../../secret_file HTTP/1.1`)
* `(.html|.png)?` forces resource requests to either have **no extension**, having **.html** extension or having **.png** extension (where no extension requests are later defaulted to **.html**)

Although the expression allows for directory requests, like `GET / HTTP/1.1` or `GET /instructions/ HTTP/1.1`, the server interprets these as an implicit index.html request, like `GET /index.html HTTP/1.1` or `GET /instructions/index.html HTTP/1.1` (respectively).

## 1. Instructions

There's not much to know about this server when simply using it.

As and admin with access to the server, as soon as you fire it up you are presented with a menu and all the available options. You can **Run** and **Stop** the server as many times as you want and you don't need to **Stop** it before you **Exit** it (all necessary operations will be taken care of in the background). Lastly, **Hist** will simply show you some information about running statistics, in case you get curious.

As a user, there are several ways you can test it out:

* **Web Browser:** To test this server you can do it directly from your browser. By default the server is initialized at **port 9000** so, by running it locally, you can connect your browser to `localhost:9000` and check out the webpages I created. If you run it remotely just make sure to adjust the **hostname** and **port** accordingly. **NOTE:** favicons will not be displayed by your browser when running the server locally.

* **Manually:** You can also issue your own HTTP/1.1 requests manually to interact with the server, porvided they are formatted in a way the server considers **valid**. In which case, you will probably see the verbose version of the HTML pages (but here you can force the connection to be kept-alive and request several pages through the same socket).

## 2. Logging

[Log4j](https://logging.apache.org/log4j/2.x/) will allow you to keep track of more intricate details of this server. By default it's disabled but it comes pre-configured **command-line** and/or **file** logging (personally, I would recommend the later since it won't interfere with your menu interaction). Everything can be adjusted in the `log4j.properties` file.

## 3. Documentation

Since it was developed in Java, every single class/package was commented using [JavaDoc](http://www.oracle.com/technetwork/java/javase/tech/index-137868.html). To minimize the size of this server I did not include a download option in this interface but it can be generated automatically from the source code.

## 4. Sources

As indicated, below follow the information sources used to create the server. Although I searched for which classes to use, I never took snippets of code from forums or websites. The whole code was created from scratch based on the classes' official documentation.

|Resource|Links|Description|
|:--:|:--:|:--:|
|Maven|[Official Page](https://maven.apache.org/)|Manually configured according to specification.|
|Log4j|[Official Page](https://logging.apache.org/log4j/2.x/)|Manually configured according to specification.|
|Multi-Thread Support|[Thread class](https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html), [ExecutorService class](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html), [Future class](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Future.html)|Manually configured according to specification.|
|Thread-Pool Support|[ExecutorService class](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/ExecutorService.html), [Future class](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Future.html)|Manually configured according to specification.|
|Keep-Alive Support|[HTTP Specification](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Keep-Alive), [Socket class](https://docs.oracle.com/javase/7/docs/api/java/net/Socket.html)|Manually configured according to specification.|
|HTTP Response Support|[HTTP Specification](https://www.w3.org/Protocols/rfc2616/rfc2616-sec6.html), [MIME Types](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Complete_list_of_MIME_types), [Status Codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status), [Examples](https://www.tutorialspoint.com/http/http_responses.htm)|Created from specification and checked with online examples.|
|HTML Pages|[Bootstrap](https://getbootstrap.com/docs/3.3/getting-started/)|Manually configured according to specification.|
|HTML Simplification|[HTML Minifier](https://code.google.com/archive/p/htmlcompressor/)|Used to reduce package size by simplifying HTML pages' content.|

Although a bit more work than building on top of already built libraries (or projects) I also wanted to take this chance to learn a bit more certain aspects of a project like this - hence delving more the documentation instead of [stackoverflow.com](https://stackoverflow.com/).

## 5. Final comments

There are things that definitely could be improved in the project. One of the things would definitely be proper **unit testing** (with libraries like [JUnit](https://junit.org/junit5/)). Due to the advised *one week* period I was given for this assignment, I assumed I wasn't expected to delve into this. Since the only tests I created were very simple - mainly targeting specific features I wanted to implement - they were ultimately discarded as they merely worked as a more technical **TODO** list. Regardless, this would be one of the things to focus on in the next stages.